# WDFpipe website documentation


The website has been created in the Jekyll framework, beautiful Jekyll template.

Jekyll has been written in Ruby and allows for the edition of websites in Markdown
language, instead of raw HTML.

To run the following project in your computer you need to install:

- Ruby
- Jekyll
- Bundler

# Installation of environment

Before we install Jekyll, we need to make sure we have all the required dependencies.
The following commands are suited for Ubuntu.

```
sudo apt-get install ruby-full build-essential zlib1g-dev
```

It is best to avoid installing Ruby Gems as the root user. Therefore, we need to set up a gem installation directory for your user account. The following commands will add environment variables to your `~/.bashrc` file to configure the gem installation path. Run them now:

```
echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

Install then Jekyll (v.3.1.2):

```
gem install jekyll -v 3.1.2
```

Then Bundler:

```
gem install bundler
```

# Docker

Alternatively it is possible to create the Docker image using attached `Dockerfile` and develop project inside.

# Configuration of the websites

Before you build the project, you need to install dependencies. Run in the root directory of this project:

```
bundle install
```

# Build and tests

To build Jekyll project run:

```
bundle exec jekyll build
```

The project is built in the `_site` directory.

To check the website, run:

```
bundle exec jekyll serve
```

This command will run Jekyll server in the background. It is accessible on the `localhost:4000`.
Don't open `index.html` - it will contain only plain html content.

# Edition of the websites

To add the page to the website, create new markdown files in the `content` directory.
Important! Add this header to each file, modifying the title and subtitle accordingly:

```
---
layout: page
title: License
subtitle: Use of WDFpipe
---
```

Then you can add new entry to the `_config.yml` file in the `navbar-links` field so the page will be
accessible from the navigation bar.

If you want to modify the main page, you can do this in `index.md` file.
