---
layout: page
title: Wavelet Detection Filter
subtitle: An opensource Python library for the gravitational waves data analysis based on wavelet transform
order: 1
---


![Crepe](/img/reconstruction.png){: .center-block :}

Wavelet Detection Filter is an open source Python package devised to search
for a various transient signals detected by gravitational waves detectors. Among those
signals are gravitational waves but also instrumental artifacts called glitches.
As a search pipeline, WDF utilizes Wavelet decomposition and de-noising of initial
signal.
