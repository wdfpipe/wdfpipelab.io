---
layout: page
title: WDF
subtitle: Wavelet Detection Filter
---


[Wavelet Detection Filter](https://gitlab.com/wdfpipe/wdf) is a Python package utilizing p4TSA for the transient signal detection and classification in the gravitational waves data.
