---
layout: page
title: Overview
---
The **Wavelet Detection Filter** (WDF) pipeline is a signal processing tool in the form of an open source Python package. It was initially built to search for transient signals detected by gravitational waves detectors, but it can be used for analysis of transient signals present in time series of any nature and kind. The WDF merges two different tools:

1.  **p4TSA**: a C++ library with python binding which contains functions for time domain whitening and wavelet decomposition of data written Hierarchical Data Format (.h5) and Gravitational Wave Frame files (.gwf, .ffl)
2.  **WDF**: a python frontend to p4TSA to analyse time series and produce GPS triggers, along with signal-to-noise ratio, frequencies and wavelet coefficients for reconstruction.
