---
layout: page
title: License
subtitle: Use of WDF
---

If you use WDF in your scientific publications or projects, we ask that you acknowlege our work by citing the publications that describe WDF and the software’s digital object identifier, as described in the WDF citation guidelines.

```
GNU GENERAL PUBLIC LICENSE
   Version 3, 29 June 2007
```
