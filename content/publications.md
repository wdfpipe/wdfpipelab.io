---
layout: page
title: Publications
---
We hereby provide publications which describe the whitening pipeline:


1.   E. Cuoco, G. Calamai, L. Fabbroni, G. Losurdo, M. Mazzoni, R. Stanga and F. Vetrano, *On-line power spectra identification and whitening for the noise in interferometric gravitational wave detectors*, 2001, Class. Quantum Grav. **18** 1727
2.   E. Cuoco, M. Razzano and A. Utina, "Wavelet-Based Classification of Transient Signals for Gravitational Wave Detectors," 2018 26th European Signal Processing Conference (EUSIPCO), Rome, 2018, pp. 2648-2652.

Here you can find a some of publications where WDF pipeline was used to obtain scientific results:

1.   J. Powell, D. Trifirò, E. Cuoco, I.S. Heng and Marco Cavaglià, *Classification methods for noise transients in advanced gravitational-wave detectors*, 2015, Class. Quantum Grav. **32** 215012
2.   J. Powell, A. Torres-Forné, R. Lynch, D. Trifirò, E. Cuoco, M. Cavaglià, I.S. Heng and J.A. Font2, *Classification methods for noise transients in advanced gravitational-wave detectors II: performance tests on Advanced LIGO data*, 2017, Class. Quantum Grav. **34** 034002
3.   M. Razzano, E. Cuoco, *Image-based deep learning for classification of noise transients in gravitational wave detectors*, 2018, Class. Quantum Grav. **35** 095016 
4.   A. Iess, E. Cuoco, F. Morawski, J. Powell, *Core-Collapse Supernova gravitational-wave search and deep learning classification*, 2020, Mach. Learn. Sci. Technol.