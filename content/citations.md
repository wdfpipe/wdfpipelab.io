---
layout: page
title: Citations
subtitle: Citing the scientific publications that describe WDF
---

The WDF is free to use. However we request that publications and other work arising from use of this code cite the following publications:

```
@inproceedings{inproceedings,
author = {Cuoco, Elena and Razzano, Massimiliano and Utina, Andrei},
year = {2018},
month = {09},
pages = {2648-2652},
title = {Wavelet-Based Classification of Transient Signals for Gravitational Wave Detectors},
doi = {10.23919/EUSIPCO.2018.8553393}
}
```

```
@article{article,
author = {Cuoco, Elena and Calamai, Giovanni and Fabbroni, and Losurdo, Giovanni and Mazzoni, Massimo and Stanga, Ruggero and Vetrano, Filippo},
year = {2001},
month = {04},
pages = {1727},
title = {On-line power spectra identification and whitening for the noise in interferometric gravitational wave detectors},
volume = {18},
journal = {Classical and Quantum Gravity},
doi = {10.1088/0264-9381/18/9/309}
}
```

We also request that software developed from this code respository refers the URL of this project and indicates the code version (via e.g., tag and/or git hash) used for development.
