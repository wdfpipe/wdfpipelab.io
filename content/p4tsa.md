---
layout: page
title: p4TSA
subtitle: Python package for the time-series analysis
---

The core functionality of WDF rests on the [Package for Time Series Analysis](https://github.com/elenacuoco/p4TSA) library.

p4TSA is a minimal package containing ad hoc function to work with time series.
It includes:
  - Whitening (Double Whitening) in time domain
  - Wavelet decomposition
  - Wavelet Detection Filter
